# DRKA

<img src='Fig2.png'>

A Text-enhanced Knowledge Graph embedding framework which augments KG embeddings with multiple text descriptions.
DKRA jointly learns to retrieve text descriptions relevant to a KG triple and align these descriptions to a KG entity embedding. 

## DATA

DRKA uses the [Google news dataset](https://code.google.com/archive/p/word2vec/), Freebase [FB15](https://pan.baidu.com/s/1eSvyY46) and [babelnet](https://old.datahub.io/dataset/babelnet)
